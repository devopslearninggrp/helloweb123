package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;

public class AboutTest {

        @Test
        public void testdesc() throws Exception {
		About obj = new About();
		String result = obj.desc();
		String expectedResult = "This application was copied from somewhere,secret sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!";
		assertEquals(expectedResult, result);

        }

}

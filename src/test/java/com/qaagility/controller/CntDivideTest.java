package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;

public class CntDivideTest {               
        @Test
    public void testDivisionByZero() {
        CntDivide cnt = new CntDivide();
        int result = cnt.divide(10, 0);
        assertEquals(Integer.MAX_VALUE, result);
    }

    @Test
    public void testPositiveDivision() {
        CntDivide cnt = new CntDivide();
        int result = cnt.divide(10, 2);
        assertEquals(5, result);
    }

    @Test
    public void testNegativeDivision() {
        CntDivide cnt = new CntDivide();
        int result = cnt.divide(-10, 2);
        assertEquals(-5, result);
    }

    @Test
    public void testDivisionResultingInZero() {
        CntDivide cnt = new CntDivide();
        int result = cnt.divide(0, 10);
        assertEquals(0, result);
    }

    @Test
    public void testDivisionByNegativeNumber() {
        CntDivide cnt = new CntDivide();
        int result = cnt.divide(10, -2);
        assertEquals(-5, result);
    }

}
